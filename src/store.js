import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";
import authsReducer from "./reducers/auth.reducer";
import { persistReducer } from "redux-persist";
import { configureStore } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import userReducer from "./reducers/user.reducer";

const reducers = combineReducers({
  auths: authsReducer,
  user: userReducer,
});

const rootReducer = (state, action) => {
  if (action.type === "auths/logout") {
    state = undefined;
  }
  return reducers(state, action);
};

const persistConfig = {
  key: "template",
  storage,
  blacklist: [""],
  whitelist: ["auths"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  devTools: true,
  middleware: [thunk],
});

export default store;
