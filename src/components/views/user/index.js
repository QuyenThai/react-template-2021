import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Dashboard from "../../pages/dashboard/Dashboard";

export default function User() {
  return (
    <Switch>
      <Route exact path="/user/dashboard" component={Dashboard} />

      <Redirect to="/user/dashboard" />
    </Switch>
  );
}
