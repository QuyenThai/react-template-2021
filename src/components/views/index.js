import React from "react";
import { Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

const Main = () => {
  const user = useSelector((state) => state.auths.user);
  const { role } = user;
  let redirectPath;
  if (user) {
    if (role === "user") {
      redirectPath = "/user";
    } else if (role === "admin") {
      redirectPath = "/admin";
    } else if (role === "sub") {
      redirectPath = "/sub";
    }
  }
  if (redirectPath) {
    return <Redirect to={redirectPath} />;
  }
  return <Redirect to="/home" />;
};

export default Main;
