import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Dashboard from "../../pages/dashboard/Dashboard";

export default function Admin() {
  console.log("hello");
  return (
    <Switch>
      <Route exact path="/admin/dashboard" component={Dashboard} />

      <Redirect to="/admin/dashboard" />
    </Switch>
  );
}
