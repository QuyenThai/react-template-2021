import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import LockScreen from "./Lockscreen";
import { useSelector } from "react-redux";
import moment from "moment";

function Authorize() {
  const auths = useSelector((state) => state.auths);
  const { token, user } = auths;
  if (token && user && moment(token.expiresIn) > moment()) {
    return <Redirect to="/" />;
  }
  return (
    <Switch>
      <Route path="/auths/login" component={Login} />
      <Route path="/auths/register" component={Register} />
      <Route path="/auths/lockscreen" component={LockScreen} />
      <Redirect from="/auths" to={`auths/lockscreen`} />
    </Switch>
  );
}

export default Authorize;
