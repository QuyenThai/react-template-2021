import React from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import * as Yup from "yup";
import { Formik, Field, ErrorMessage, Form } from "formik";
import { useRegisterMutation } from "../../../apis/auth.api";

const registerSchema = Yup.object().shape({
  email: Yup.string().email().required(),
  username: Yup.string().required().min(6).max(20),
  password: Yup.string().required().min(6).max(20),
});

function Register() {
  const [register, { isError, isLoading, isSuccess }] = useRegisterMutation();
  return (
    <div>
      <div className="d-flex align-items-center auth px-0">
        <div className="row w-100 mx-0">
          <div className="col-lg-4 mx-auto">
            <div className="auth-form-light text-left py-5 px-4 px-sm-5">
              <div className="brand-logo">
                <img
                  src={require("../../../assets/images/logo.svg")}
                  alt="logo"
                />
              </div>
              <h4>New here?</h4>
              <h6 className="font-weight-light">
                Signing up is easy. It only takes a few steps
              </h6>
              <Formik
                initialValues={{ email: "", username: "", password: "" }}
                validationSchema={registerSchema}
                onSubmit={async (values) => {
                  console.log("values: ", values);
                  const { email, username, password } = values;

                  const response = await register({
                    email,
                    username,
                    password,
                    role: "user",
                  });
                  console.log("response: ", response);
                }}
              >
                {() => (
                  <Form>
                    <div className="form-group">
                      <label htmlFor="email">Email</label>
                      <Field
                        name="email"
                        className="form-control"
                        type="text"
                      />
                      <ErrorMessage
                        name="email"
                        component="div"
                        className="invalid-feedback d-block"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="username">Username</label>
                      <Field
                        name="username"
                        className="form-control"
                        type="text"
                      />
                      <ErrorMessage
                        name="username"
                        component="div"
                        className="invalid-feedback d-block"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="password">Password</label>
                      <Field
                        name="password"
                        className="form-control"
                        type="text"
                      />
                      <ErrorMessage
                        name="password"
                        component="div"
                        className="invalid-feedback d-block"
                      />
                    </div>
                    <div className="mb-4">
                      <div className="form-check">
                        <label className="form-check-label text-muted">
                          <input type="checkbox" className="form-check-input" />
                          <i className="input-helper"></i>I agree to all Terms &
                          Conditions
                        </label>
                      </div>
                    </div>
                    <div className="mt-3">
                      <Button
                        className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                        type="submit"
                        disabled={isLoading}
                      >
                        SIGN UP
                      </Button>
                    </div>
                    <div className="text-center mt-4 font-weight-light">
                      Already have an account?{" "}
                      <Link to="/auths/login" className="text-primary">
                        Login
                      </Link>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
