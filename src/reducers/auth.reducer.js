import { createSlice } from "@reduxjs/toolkit";
import { authApi } from "../apis/auth.api";

const initialValue = { user: null, token: null, keepInsign: false };

const authSlice = createSlice({
  name: "auths",
  initialState: initialValue,
  reducers: {
    [authApi.reducerPath]: authApi.reducer,

    logout: (state, action) => {
      return initialValue;
    },

    middleware: (getDefaultMiddleware) => {
      return getDefaultMiddleware().concat(authApi.middleware);
    },
  },
  extraReducers: (builder) => {
    // Xử lý logic khi endpoint login được fulfilled
    builder.addMatcher(
      authApi.endpoints.login.matchFulfilled,
      (state, action) => {
        // Lưu thông tin user vào state
        state.user = action.payload.user;
        state.token = action.payload.token;
      }
    );
    builder.addMatcher(
      authApi.endpoints.register.matchFulfilled,
      (state, action) => {
        // Lưu thông tin user vào state
        state.user = action.payload.user;
        state.token = action.payload.token;
      }
    );
  },
});

export const { logout } = authSlice.actions;

export default authSlice.reducer;
