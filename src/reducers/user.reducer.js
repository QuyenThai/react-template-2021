import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "auths",
  initialState: { user: null, token: null, keepInsign: false },
  reducers: {
    change: (state, action) => {
      console.log("state: ", state);
    },
  },
});

export const { change } = userSlice.actions;

export default userSlice.reducer;
