import React, { Suspense, lazy } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import moment from "moment";

import Spinner from "./components/shared/Spinner";
import Dashboard from "./components/pages/dashboard/Dashboard";
import Login from "./components/pages/user-pages/Login";
import Register from "./components/pages/user-pages/Register";
import Error404 from "./components/pages/error-pages/Error404";
import Error500 from "./components/pages/error-pages/Error500";
import Authorize from "./components/pages/user-pages/index";
import Main from "./components/views";
import Admin from "./components/views/admin";
import User from "./components/views/user";

const Buttons = lazy(() => import("./components/pages/basic-ui/Buttons"));
const Dropdowns = lazy(() => import("./components/pages/basic-ui/Dropdowns"));
const Typography = lazy(() => import("./components/pages/basic-ui/Typography"));
const BasicElements = lazy(() =>
  import("./components/pages/form-elements/BasicElements")
);
const BasicTable = lazy(() => import("./components/pages/tables/BasicTable"));
const Mdi = lazy(() => import("./components/pages/icons/Mdi"));
const ChartJs = lazy(() => import("./components/pages/charts/ChartJs"));
const Lockscreen = lazy(() =>
  import("./components/pages/user-pages/Lockscreen")
);
const BlankPage = lazy(() =>
  import("./components/pages/general-pages/BlankPage")
);

const PrivateRoute = ({ component: Component, roles, auths, path }) => {
  const { user, token } = auths;

  return (
    <Route
      path={path}
      render={(props) => {
        if (token) {
          const isExpiresIn = moment(token.expiresIn) > moment();

          if (!isExpiresIn) {
            return (
              <Redirect
                to={{
                  pathname: "/auths/login",
                  state: { from: props.location },
                }}
              />
            );
          }
          if (roles && roles.includes(user.role)) {
            return <Component />;
          }
          return <Redirect to={{ pathname: "/" }} />;
        }
        return (
          <Redirect
            to={{ pathname: "/auths/login", state: { from: props.location } }}
          />
        );
      }}
    />
  );
};

const AppRoutes = () => {
  const auths = useSelector((state) => state.auths);
  return (
    <Suspense fallback={<Spinner />}>
      <Switch>
        <PrivateRoute
          path="/user"
          roles={["user"]}
          auths={auths}
          component={User}
        />
        <PrivateRoute
          path="/admin"
          roles={["admin"]}
          auths={auths}
          component={Admin}
        />
        <Route path="/" exact component={Main} />
        <Route path="/auths" component={Authorize} />
        <Route exact path="/dashboard" component={Dashboard} />

        {/* <Route path="/basic-ui/buttons" component={Buttons} />
        <Route path="/basic-ui/dropdowns" component={Dropdowns} />
        <Route path="/basic-ui/typography" component={Typography} />

        <Route path="/form-Elements/basic-elements" component={BasicElements} />

        <Route path="/tables/basic-table" component={BasicTable} />

        <Route path="/icons/mdi" component={Mdi} />

        <Route path="/charts/chart-js" component={ChartJs} />

        <Route path="/user-pages/login-1" component={Login} />
        <Route path="/user-pages/register-1" component={Register} />
        <Route path="/user-pages/lockscreen" component={Lockscreen} />

        <Route path="/error-pages/error-404" component={Error404} />
        <Route path="/error-pages/error-500" component={Error500} />

        <Route path="/general-pages/blank-page" component={BlankPage} /> */}

        <Redirect to="/error" />
      </Switch>
    </Suspense>
  );
};

// const PrivateRoute = ({ component: Component, roles, loginUser, path }) => (
//   <Route
//     path={path}
//     render={(props) => {
//       if (isAuthenticated) {
//         if (roles && roles.indexOf(loginUser.role) === -1) {
//           return <Redirect to={{ pathname: "/" }} />;
//         }
//         return <Component />;
//       }
//       return (
//         <Redirect to={{ pathname: "/", state: { from: props.location } }} />
//       );
//     }}
//   />
// );

// function PrivateRoute({ component, ...rest }) {
//   return (
//     <Route
//       {...rest}
//       render={(props) =>
//         isAuthenticated ? (
//           React.createElement(component, props)
//         ) : (
//           <Redirect
//             to={{
//               pathname: "/login",
//               state: {
//                 from: props.location,
//               },
//             }}
//           />
//         )
//       }
//     />
//   );
// }

// function PublicRoute({ component, ...rest }) {
//   return (
//     <Route
//       {...rest}
//       render={(props) =>
//         isAuthenticated ? (
//           <Redirect
//             to={{
//               pathname: "/",
//             }}
//           />
//         ) : (
//           React.createElement(component, props)
//         )
//       }
//     />
//   );
// }

export default AppRoutes;
